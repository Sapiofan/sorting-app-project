import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains logic to sort array of numbers
 */
public class SortingApp {
    /**
     * @param args String array of Integer representation
     * @throw IllegalArgumentException when number of arguments more than 10
     */
    public static void main(String[] args) {
        if (args.length > 10) {
            throw new IllegalArgumentException("Arguments more than 10");
        }

        List<Integer> integers = new ArrayList<>();

        for (String arg : args) {
            integers.add(Integer.parseInt(arg));
        }

        Collections.sort(integers);
        for (Integer integer : integers) {
            System.out.print(integer + " ");
        }
    }
}
