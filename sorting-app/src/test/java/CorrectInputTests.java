import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CorrectInputTests {

    private final String expected;
    private final String[] input;

    public CorrectInputTests(String expected, String[] input) {
        this.expected = expected;
        this.input = input;
    }

    /**
     * check main function when expected no exceptions will not be thrown and
     * function output correct result in console
     */
    @Test
    public void correctResult() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        SortingApp.main(input);
        Assert.assertEquals(expected, out.toString("UTF-8").trim());
    }

    @Parameters
    public static Collection<Object> data() {
        String[] arr1 = {"3", "2", "4"};
        String[] arr2 = {"3", "-5", "28", "-3"};
        String[] arr3 = {"5", "5", "4"};
        String[] arr4 = {"3", "2", "1"};
        return Arrays.asList(new Object[][]{
                {"2 3 4", arr1}, {"-5 -3 3 28", arr2},
                {"4 5 5", arr3}, {"1 2 3", arr4},
        });
    }

}
