import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class WrongInputTests {
    /**
     * Check main function for output of empty string
     */
    @Test
    public void zeroArgumentsTest() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        String[] arr = new String[0];
        SortingApp.main(arr);
        Assert.assertEquals("", out.toString("UTF-8").trim());
    }

    /**
     * Check main function for output of 1 number
     */
    @Test
    public void oneArgumentTest() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        String[] arr = {"1"};
        SortingApp.main(arr);
        Assert.assertEquals("1", out.toString("UTF-8").trim());
    }

    /**
     * Check main function for output of 10 numbers
     */
    @Test
    public void tenArgumentsTest() throws UnsupportedEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
        String[] arr = {"10", "2", "3", "1", "7", "5", "1", "3", "8", "1"};
        SortingApp.main(arr);
        Assert.assertEquals("1 1 1 2 3 3 5 7 8 10", out.toString("UTF-8").trim());
    }

    /**
     * Expected IllegalArgumentException because number of argument more than 10
     */
    @Test(expected = IllegalArgumentException.class)
    public void moreThanTenArgumentsTest() {
        String[] arr = {"10", "2", "3", "1", "7", "5", "1", "3", "8", "1", "11"};
        SortingApp.main(arr);
    }

    /**
     * Expected NumberFormatException because Integer.parseInt() can't parse the string into number
     */
    @Test(expected = NumberFormatException.class)
    public void notNumberArgumentsTest() {
        String[] arr = {"10", "str"};
        SortingApp.main(arr);
    }
}
